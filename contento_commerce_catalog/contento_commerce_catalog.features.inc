<?php
/**
 * @file
 * contento_commerce_catalog.features.inc
 */

/**
 * Implements hook_views_api().
 */
function contento_commerce_catalog_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
